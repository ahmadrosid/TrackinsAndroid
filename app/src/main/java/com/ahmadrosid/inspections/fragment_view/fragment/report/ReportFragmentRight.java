package com.ahmadrosid.inspections.fragment_view.fragment.report;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.activity.MainActivity;
import com.ahmadrosid.inspections.activity.view.ReportActivityView;
import com.ahmadrosid.inspections.fragment_view.presenter.report.ReportFragmentRightPresenter;
import com.ahmadrosid.inspections.fragment_view.view.report.FragmentLeftRightReportView;
import com.ahmadrosid.inspections.helper.PickImageHelper;
import com.ahmadrosid.inspections.helper.ProgressLoader;
import com.ahmadrosid.inspections.helper.SetToolbar;
import com.ahmadrosid.inspections.ui.FormInputImage;
import com.ahmadrosid.inspections.utils.PermissionHelper;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ahmadrosid.inspections.helper.Constants.REQUEST_CAMERA;
import static com.ahmadrosid.inspections.helper.Constants.REQUEST_GALLERY;

/**
 * Created by ocittwo on 10/21/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class ReportFragmentRight extends Fragment implements FragmentLeftRightReportView{

    private ReportActivityView reportActivityView;
    private ReportFragmentRightPresenter presenter;
    private PickImageHelper pickImageHelper;
    private PermissionHelper permission;
    private ArrayList<FormInputImage> arrayForm;
    private FormInputImage currentForm;
    private ProgressLoader loader;

    @BindView(R.id.right_side_link) FormInputImage right_side_link;
    @BindView(R.id.right_side_bushing) FormInputImage right_side_bushing;
    @BindView(R.id.right_side_track_shoe) FormInputImage right_side_track_shoe;
    @BindView(R.id.right_front_idler) FormInputImage right_front_idler;
    @BindView(R.id.right_rear_idler) FormInputImage right_rear_idler;
    @BindView(R.id.right_sprocket) FormInputImage right_sprocket;
    @BindView(R.id.right_one_carrier_roller) FormInputImage right_one_carrier_roller;
    @BindView(R.id.right_two_carrier_roller) FormInputImage right_two_carrier_roller;
    @BindView(R.id.right_three_carrier_roller) FormInputImage right_three_carrier_roller;
    @BindView(R.id.right_one_track_roller) FormInputImage right_one_track_roller;
    @BindView(R.id.right_two_track_roller) FormInputImage right_two_track_roller;
    @BindView(R.id.right_three_track_roller) FormInputImage right_three_track_roller;
    @BindView(R.id.right_four_track_roller) FormInputImage right_four_track_roller;
    @BindView(R.id.right_five_track_roller) FormInputImage right_five_track_roller;
    @BindView(R.id.right_six_track_roller) FormInputImage right_six_track_roller;
    @BindView(R.id.right_seven_track_roller) FormInputImage right_seven_track_roller;
    @BindView(R.id.right_eight_track_roller) FormInputImage right_eight_track_roller;
    @BindView(R.id.right_nine_track_roller) FormInputImage right_nine_track_roller;
    @BindView(R.id.right_ten_track_roller) FormInputImage right_ten_track_roller;
    @BindView(R.id.track_sag_right) FormInputImage track_sag_right;
    @BindView(R.id.track_sag_right_status) FormInputImage track_sag_right_status;
    @BindView(R.id.dry_joint_right) FormInputImage dry_joint_right;
    @BindView(R.id.frame_extension_right) FormInputImage frame_extension_right;

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report_right, container, false);
        ButterKnife.bind(this, view);
        setupPresenter();
        return view;
    }

    private void setupPresenter() {
        arrayForm = new ArrayList<>();
        arrayForm.add(right_side_link);
        arrayForm.add(right_side_bushing);
        arrayForm.add(right_side_track_shoe);
        arrayForm.add(right_front_idler);
        arrayForm.add(right_rear_idler);
        arrayForm.add(right_sprocket);
        arrayForm.add(right_one_carrier_roller);
        arrayForm.add(right_two_carrier_roller);
        arrayForm.add(right_three_carrier_roller);
        arrayForm.add(right_one_track_roller);
        arrayForm.add(right_two_track_roller);
        arrayForm.add(right_three_track_roller);
        arrayForm.add(right_four_track_roller);
        arrayForm.add(right_five_track_roller);
        arrayForm.add(right_six_track_roller);
        arrayForm.add(right_seven_track_roller);
        arrayForm.add(right_eight_track_roller);
        arrayForm.add(right_nine_track_roller);
        arrayForm.add(right_ten_track_roller);
        arrayForm.add(track_sag_right);
        arrayForm.add(track_sag_right_status);
        arrayForm.add(dry_joint_right);
        arrayForm.add(frame_extension_right);

        permission = new PermissionHelper(getActivity());
        pickImageHelper = new PickImageHelper(getContext());
        presenter = new ReportFragmentRightPresenter(this, arrayForm);
        presenter.setPickImage();
    }

    @Override public void onStart() {
        if (reportActivityView == null)
            reportActivityView = (ReportActivityView) getActivity();
        super.onStart();
    }

    @Override public void onResume() {
        SetToolbar.setTitleFragment(this, "RIGHT");
        presenter.onAttachView(this);
        presenter.resumeFragment(arrayForm);
        if (reportActivityView == null)
            reportActivityView = (ReportActivityView) getActivity();
        super.onResume();
    }

    @Override public void onStop() {
        super.onStop();
        reportActivityView = null;
    }

    @Override public void pickImageFromGallery(int requestCode) {
        if (permission.isSdkVersionM()) {
            reportActivityView.requestPermissionGallery(() -> startOpenCamera(requestCode));
        } else {
            startOpenGallery(requestCode);
        }
    }

    private void startOpenGallery(int requestCode) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), requestCode);
    }


    @Override public void pickImageFromCamera(int requestCode) {
        if (permission.isSdkVersionM()) {
            reportActivityView.requestPermissionCamera(() -> startOpenCamera(requestCode));
        } else {
            startOpenCamera(requestCode);
        }
    }

    private void startOpenCamera(int requestCode) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = pickImageHelper.createImageFile(requestCode);
            if (photoFile != null) {
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, requestCode);
            }
        }
    }

    @Override public void setCurrentForm(FormInputImage formInputImage) {
        this.currentForm = formInputImage;
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    pickImageHelper.setImgFromCamera(currentForm.getImageView(), path -> currentForm.setImgPath(path));
                    break;
                case REQUEST_GALLERY:
                    pickImageHelper.setImgFromGallery(data, currentForm.getImageView(), path -> currentForm.setImgPath(path));
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
            case R.id.menu_next:
                clickNext();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_next) void clickNext() {
        if (presenter.finish()) {
            loader = new ProgressLoader(getContext());
            loader.showProgressDialog();
            new Handler().postDelayed(this::goToHome, 2500);
        }
    }

    private void goToHome() {
        presenter.cache(arrayForm);
        Intent intent = new Intent(getContext(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override public void onDestroy() {
        presenter.cache(arrayForm);
        super.onDestroy();
    }

}
