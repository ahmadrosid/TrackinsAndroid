package com.ahmadrosid.inspections.fragment_view.view.login;

/**
 * Created by ocittwo.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 *
 * © 2016 | All Rights Reserved
 */
public interface FragmentLoginView {
    void showMessage(String s);
    void showProgress();
    void hideProgress();
    void successLogin();
    void register();
}