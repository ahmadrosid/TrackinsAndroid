package com.ahmadrosid.inspections.fragment_view.model;

/**
 * Created by ocittwo on 27/09/16.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 *
 * © 2016 | App All Rights Reserved
 */
public class User {
    public String username;
    public String email;
    public String password;

    public User(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

}
