package com.ahmadrosid.inspections.fragment_view.presenter.home;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.ui.RecyclerViewAdapter;
import com.ahmadrosid.inspections.fragment_view.fragment.FragmentNotifications;
import com.ahmadrosid.inspections.holder.HolderNotification;

import java.util.ArrayList;

/**
 * Created by ocittwo on 10/4/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class FragmentNotificationsPresenter {
    private final FragmentNotifications fragment;
    private final Context context;

    private ArrayList<String> data;

    public FragmentNotificationsPresenter(FragmentNotifications fragment) {
        this.fragment = fragment;
        this.context = fragment.getContext();
    }

    public void setLisNotification(RecyclerView list) {
        data = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            data.add("New Data");
        }

        RecyclerViewAdapter<String, HolderNotification> adapter = new RecyclerViewAdapter<String, HolderNotification>(data, String.class, R.layout.item_list_notification, HolderNotification.class) {
            @Override protected void executeViewHolder(HolderNotification viewHolder, String model, int position) {

            }
        };

        list.setLayoutManager(new LinearLayoutManager(context));
        list.setAdapter(adapter);
    }
}
