package com.ahmadrosid.inspections.fragment_view.fragment.report;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.activity.view.ReportActivityView;
import com.ahmadrosid.inspections.fragment_view.presenter.report.ReportFragmentLeftPresenter;
import com.ahmadrosid.inspections.fragment_view.view.report.FragmentLeftRightReportView;
import com.ahmadrosid.inspections.helper.PickImageHelper;
import com.ahmadrosid.inspections.helper.ProgressLoader;
import com.ahmadrosid.inspections.helper.SetToolbar;
import com.ahmadrosid.inspections.ui.FormInputImage;
import com.ahmadrosid.inspections.utils.PermissionHelper;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ahmadrosid.inspections.helper.Constants.REQUEST_CAMERA;
import static com.ahmadrosid.inspections.helper.Constants.REQUEST_GALLERY;

/**
 * Created by ocittwo on 10/8/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class ReportFragmentLeft extends Fragment implements FragmentLeftRightReportView {

    private ReportActivityView reportActivityView;
    private ReportFragmentLeftPresenter presenter;
    private PickImageHelper pickImageHelper;
    private PermissionHelper permission;
    private ArrayList<FormInputImage> arrayForm;
    private FormInputImage currentForm;
    private ProgressLoader loader;

    @BindView(R.id.left_side_link) FormInputImage left_side_link;
    @BindView(R.id.left_side_bushing) FormInputImage left_side_bushing;
    @BindView(R.id.left_side_track_shoe) FormInputImage left_side_track_shoe;
    @BindView(R.id.left_front_idler) FormInputImage left_front_idler;
    @BindView(R.id.left_rear_idler) FormInputImage left_rear_idler;
    @BindView(R.id.left_sprocket) FormInputImage left_sprocket;
    @BindView(R.id.left_one_carrier_roller) FormInputImage left_one_carrier_roller;
    @BindView(R.id.left_two_carrier_roller) FormInputImage left_two_carrier_roller;
    @BindView(R.id.left_three_carrier_roller) FormInputImage left_three_carrier_roller;
    @BindView(R.id.left_one_track_roller) FormInputImage left_one_track_roller;
    @BindView(R.id.left_two_track_roller) FormInputImage left_two_track_roller;
    @BindView(R.id.left_three_track_roller) FormInputImage left_three_track_roller;
    @BindView(R.id.left_four_track_roller) FormInputImage left_four_track_roller;
    @BindView(R.id.left_five_track_roller) FormInputImage left_five_track_roller;
    @BindView(R.id.left_six_track_roller) FormInputImage left_six_track_roller;
    @BindView(R.id.left_seven_track_roller) FormInputImage left_seven_track_roller;
    @BindView(R.id.left_eight_track_roller) FormInputImage left_eight_track_roller;
    @BindView(R.id.left_nine_track_roller) FormInputImage left_nine_track_roller;
    @BindView(R.id.left_ten_track_roller) FormInputImage left_ten_track_roller;
    @BindView(R.id.track_sag_left) FormInputImage track_sag_left;
    @BindView(R.id.track_sag_left_status) FormInputImage track_sag_left_status;
    @BindView(R.id.dry_joint_left) FormInputImage dry_joint_left;
    @BindView(R.id.frame_extension_left) FormInputImage frame_extension_left;

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_report_left, container, false);
        ButterKnife.bind(this, view);
        setupPresenter();
        if (reportActivityView == null)
            reportActivityView = (ReportActivityView) getActivity();
        reportActivityView.setTitleToolbar("LEFT");
        return view;
    }

    @Override public void onStart() {
        Log.d(TAG, "onStart: opened!");
        presenter.onAttachView(this);
        if (reportActivityView == null)
            reportActivityView = (ReportActivityView) getActivity();
        super.onStart();
    }

    private static final String TAG = "ReportFragmentLeft";

    @Override public void onResume() {
        Log.d(TAG, "onResume: Opened!");
        presenter.onAttachView(this);
        SetToolbar.setTitleFragment(this, "LEFT");
        presenter.resumeFragment(arrayForm);
        if (reportActivityView == null)
            reportActivityView = (ReportActivityView) getActivity();
        super.onResume();
    }

    @Override public void onStop() {
        super.onStop();
        presenter.onDetachView();
        reportActivityView = null;
    }

    private void setupPresenter() {
        arrayForm = new ArrayList<>();
        arrayForm.add(left_side_link);
        arrayForm.add(left_side_bushing);
        arrayForm.add(left_side_track_shoe);
        arrayForm.add(left_front_idler);
        arrayForm.add(left_rear_idler);
        arrayForm.add(left_sprocket);
        arrayForm.add(left_one_carrier_roller);
        arrayForm.add(left_two_carrier_roller);
        arrayForm.add(left_three_carrier_roller);
        arrayForm.add(left_one_track_roller);
        arrayForm.add(left_two_track_roller);
        arrayForm.add(left_three_track_roller);
        arrayForm.add(left_four_track_roller);
        arrayForm.add(left_five_track_roller);
        arrayForm.add(left_six_track_roller);
        arrayForm.add(left_seven_track_roller);
        arrayForm.add(left_eight_track_roller);
        arrayForm.add(left_nine_track_roller);
        arrayForm.add(left_ten_track_roller);
        arrayForm.add(track_sag_left);
        arrayForm.add(track_sag_left_status);
        arrayForm.add(dry_joint_left);
        arrayForm.add(frame_extension_left);

        permission = new PermissionHelper(getActivity());
        pickImageHelper = new PickImageHelper(getContext());
        presenter = new ReportFragmentLeftPresenter(this, arrayForm);
        presenter.setPickImage();

    }

    @Override public void setCurrentForm(FormInputImage formInputImage) {
        this.currentForm = formInputImage;
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    pickImageHelper.setImgFromCamera(currentForm.getImageView(), path -> currentForm.setImgPath(path));
                    break;
                case REQUEST_GALLERY:
                    pickImageHelper.setImgFromGallery(data, currentForm.getImageView(), path -> currentForm.setImgPath(path));
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override public void pickImageFromGallery(int requestCode) {
        if (permission.isSdkVersionM()){
            reportActivityView.requestPermissionGallery(() -> startOpenGallery(requestCode));
        }else{
            startOpenGallery(requestCode);
        }
    }

    @Override public void pickImageFromCamera(int requestCode) {
        if (permission.isSdkVersionM()) {
            reportActivityView.requestPermissionCamera(() -> startOpenCamera(requestCode));
        } else {
            startOpenCamera(requestCode);
        }
    }

    private void startOpenCamera(int requestCode) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = pickImageHelper.createImageFile(requestCode);
            if (photoFile != null) {
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, requestCode);
            }
        }
    }

    private void startOpenGallery(int requestCode){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), requestCode);

    }

    @OnClick(R.id.btn_next) void clickNext() {
        if (presenter.nextToRight()) {
            loader = new ProgressLoader(getContext());
            loader.showProgressDialog();
            new Handler().postDelayed(this::goToRight, 2500);
        }
    }

    private void goToRight() {
        presenter.cache(arrayForm);
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container_report, new ReportFragmentRight())
                .addToBackStack(null)
                .commit();
        loader.hideProgressDialog();
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
            case R.id.menu_next:
                clickNext();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
