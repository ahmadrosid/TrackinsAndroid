package com.ahmadrosid.inspections.fragment_view.presenter.report;

import android.content.Context;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.ahmadrosid.inspections.data.local.TrackinsCacheManager;
import com.ahmadrosid.inspections.fragment_view.fragment.report.ReportFragmentGeneral;
import com.ahmadrosid.inspections.helper.Constants;
import com.ahmadrosid.inspections.ui.FloatLabelTextView;
import com.ahmadrosid.inspections.ui.dialog.DialogChoseLocation;
import com.ahmadrosid.inspections.ui.dialog.DialogShowMessages;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Created by ocittwo on 10/6/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class ReportFragmentGeneralPresenter {

    private final ReportFragmentGeneral reportFragmentGeneral;
    private final Context context;

    private ArrayList<FloatLabelTextView> arrayInputField;
    private TrackinsCacheManager cacheManager;

    public ReportFragmentGeneralPresenter(ReportFragmentGeneral reportFragmentGeneral, ArrayList<FloatLabelTextView> arrayInputField) {
        this.reportFragmentGeneral = reportFragmentGeneral;
        this.context = reportFragmentGeneral.getContext();
        this.arrayInputField = arrayInputField;
        cacheManager = new TrackinsCacheManager(reportFragmentGeneral.getContext());
    }

    public void setSpinnerData(Spinner spinner, int data) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
                data, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public void setDate(final FloatLabelTextView date) {
        date.getmEditText().setOnTouchListener((view, motionEvent) -> {
            date.getmEditText().setOnTouchListener((view1, motionEvent1) -> {
                switch (motionEvent1.getAction()) {
                    case MotionEvent.ACTION_UP:
                        Calendar now = Calendar.getInstance();
                        DatePickerDialog dpd = DatePickerDialog.newInstance(
                                (view11, year, monthOfYear, dayOfMonth) -> date.setText("" + year + "/" + monthOfYear + "/" + dayOfMonth),
                                now.get(Calendar.YEAR),
                                now.get(Calendar.MONTH),
                                now.get(Calendar.DAY_OF_MONTH)
                        );
                        dpd.show(reportFragmentGeneral.getActivity().getFragmentManager(), "Datepickerdialog");
                        break;
                }
                return true;
            });
            return false;
        });
    }

    public boolean validate() {
        boolean isValidate = true;
        boolean isShowEmpty = false;
        for (FloatLabelTextView floatLabelTextView : arrayInputField) {
            if (floatLabelTextView.isRequired()) {
                if (TextUtils.isEmpty(floatLabelTextView.getText().toString())) {
                    isValidate = false;
                    if (!isShowEmpty) {
                        showEmptyField(floatLabelTextView.getHint());
                        isShowEmpty = true;
                    }
                }
            }
        }
        return isValidate;
    }

    private void showEmptyField(String s) {
        DialogShowMessages.getInstance(context).setMessage("Please input " + s).show();
    }

    public void pickLocationCoordinate(FloatLabelTextView gps_coordinate) {
        gps_coordinate.getmEditText().setOnFocusChangeListener((v, hasFocus) -> startResult(hasFocus));
    }

    public void startResult(boolean hasFocus) {
        if (hasFocus) {
            DialogChoseLocation.getInstance(context).show();
        }
    }

    public void saveData(ArrayList<Spinner> arraySpinner, ArrayList<FloatLabelTextView> arrayInputField) {
        cacheManager.cacheFragmentReportGeneral(arraySpinner, arrayInputField);
    }

    public void setOnResumeData(ArrayList<Spinner> arraySpinner, ArrayList<FloatLabelTextView> arrayInputField) {
        Map<Object, List<String>> data = cacheManager.getCacheFragmentReportGeneral();
        if (data != null) {
            for (int i = 0; i < arraySpinner.size(); i++) {
                Spinner spinner = arraySpinner.get(i);
                spinner.setSelection(toInt(data.get(Constants.JSON_GENERAL_SPINNER).get(i)));
            }
            for (int i = 0; i < arrayInputField.size(); i++) {
                FloatLabelTextView inputField = arrayInputField.get(i);
                inputField.setText(data.get(Constants.JSON_GENERAL_INPUT_FIELD).get(i));
            }
        }
    }

    private int toInt(String s) {
        return Integer.parseInt(s);
    }

}
