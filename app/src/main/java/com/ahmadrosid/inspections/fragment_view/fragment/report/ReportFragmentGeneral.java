package com.ahmadrosid.inspections.fragment_view.fragment.report;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.activity.MainActivity;
import com.ahmadrosid.inspections.activity.view.ReportActivityView;
import com.ahmadrosid.inspections.fragment_view.presenter.report.ReportFragmentGeneralPresenter;
import com.ahmadrosid.inspections.helper.Constants;
import com.ahmadrosid.inspections.helper.PickImageHelper;
import com.ahmadrosid.inspections.helper.ProgressLoader;
import com.ahmadrosid.inspections.helper.SetToolbar;
import com.ahmadrosid.inspections.helper.SharedPref;
import com.ahmadrosid.inspections.ui.FloatLabelTextView;
import com.ahmadrosid.inspections.ui.dialog.DialogPickImage;
import com.ahmadrosid.inspections.ui.dialog.DialogShowMessages;
import com.ahmadrosid.inspections.utils.PermissionHelper;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ahmadrosid.inspections.helper.Constants.IMAGE_FRAGMENT_GENERAL;
import static com.ahmadrosid.inspections.helper.Constants.REQUEST_CAMERA;
import static com.ahmadrosid.inspections.helper.Constants.REQUEST_GALLERY;

/**
 * Created by ocittwo on 10/6/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class ReportFragmentGeneral extends Fragment {

    private ReportFragmentGeneralPresenter presenter;
    private PickImageHelper pickImage;

    private PermissionHelper permission;
    private ReportActivityView reportActivityView;
    private ArrayList<Spinner> arraySpinner;
    private ArrayList<FloatLabelTextView> arrayInputField;

    private String mCurrentPhotoPath;
    private ProgressLoader loader;

    @BindView(R.id.photo) ImageView photo;
    @BindView(R.id.spinner_family) Spinner family;
    @BindView(R.id.spinner_underfoot_impact) Spinner underfoot_impact;
    @BindView(R.id.spinner_underfoot_abrasive) Spinner underfoot_abrasive;
    @BindView(R.id.spinner_underfoot_moisture) Spinner underfoot_moisture;
    @BindView(R.id.spinner_underfoot_packing) Spinner underfoot_packing;
    @BindView(R.id.spinner_bushing_allowable_wear) Spinner bushing_allowable_wear;
    @BindView(R.id.spinner_link_allowable_wear) Spinner link_allowable_wear;
    @BindView(R.id.spinner_bushings_turned) Spinner bushings_turned;
    @BindView(R.id.spinner_shoe_type) Spinner shoe_type;
    @BindView(R.id.spinner_unit_of_measure) Spinner unit_of_measure;

    @BindView(R.id.customer_name) FloatLabelTextView customer_name;
    @BindView(R.id.customer_number) FloatLabelTextView customer_number;
    @BindView(R.id.job_site) FloatLabelTextView job_site;
    @BindView(R.id.gps_coordinate) FloatLabelTextView gps_coordinate;
    @BindView(R.id.model) FloatLabelTextView model;
    @BindView(R.id.model_config) FloatLabelTextView model_config;
    @BindView(R.id.serial_number) FloatLabelTextView serial_number;
    @BindView(R.id.equipment_number) FloatLabelTextView equipment_number;
    @BindView(R.id.inspection_date) FloatLabelTextView inspection_date;
    @BindView(R.id.hour_meter_reading) FloatLabelTextView hour_meter_reading;
    @BindView(R.id.hour_meter_per_week) FloatLabelTextView hour_meter_per_week;
    @BindView(R.id.notes) FloatLabelTextView notes;
    @BindView(R.id.shoe_width) FloatLabelTextView shoe_width;
    @BindView(R.id.track_roller_quantities_sf) FloatLabelTextView track_roller_quantities_sf;
    @BindView(R.id.track_roller_quantities_df) FloatLabelTextView track_roller_quantities_df;
    @BindView(R.id.track_group_pn) FloatLabelTextView track_group_pn;
    @BindView(R.id.link_assembly_pn) FloatLabelTextView link_assembly_pn;
    @BindView(R.id.track_shoe_pn) FloatLabelTextView track_shoe_pn;
    @BindView(R.id.master_shoe_pn) FloatLabelTextView master_shoe_pn;
    @BindView(R.id.carrier_roller_pn) FloatLabelTextView carrier_roller_pn;
    @BindView(R.id.front_idler_pn) FloatLabelTextView front_idler_pn;
    @BindView(R.id.rear_idler_pn) FloatLabelTextView rear_idler_pn;
    @BindView(R.id.single_flange_pn) FloatLabelTextView single_flange_pn;
    @BindView(R.id.double_flange_pn) FloatLabelTextView double_flange_pn;
    @BindView(R.id.first_sprocket_segment_pn) FloatLabelTextView first_sprocket_segment_pn;
    @BindView(R.id.seconds_sprocket_segment_pn) FloatLabelTextView seconds_sprocket_segment_pn;


    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report_general, container, false);
        ButterKnife.bind(this, view);
        setupPresenter();
        return view;
    }

    private void setupPresenter() {
        loader = new ProgressLoader(getContext());

        arraySpinner = new ArrayList<>();
        arraySpinner.add(family);
        arraySpinner.add(underfoot_impact);
        arraySpinner.add(underfoot_abrasive);
        arraySpinner.add(underfoot_moisture);
        arraySpinner.add(underfoot_packing);
        arraySpinner.add(bushing_allowable_wear);
        arraySpinner.add(link_allowable_wear);
        arraySpinner.add(bushings_turned);
        arraySpinner.add(shoe_type);
        arraySpinner.add(unit_of_measure);

        arrayInputField = new ArrayList<>();
        arrayInputField.add(customer_name);
        arrayInputField.add(customer_number);
        arrayInputField.add(job_site);
        arrayInputField.add(gps_coordinate);
        arrayInputField.add(model);
        arrayInputField.add(model_config);
        arrayInputField.add(serial_number);
        arrayInputField.add(equipment_number);
        arrayInputField.add(inspection_date);
        arrayInputField.add(hour_meter_reading);
        arrayInputField.add(hour_meter_per_week);
        arrayInputField.add(notes);
        arrayInputField.add(shoe_width);
        arrayInputField.add(track_roller_quantities_sf);
        arrayInputField.add(track_roller_quantities_df);
        arrayInputField.add(track_group_pn);
        arrayInputField.add(link_assembly_pn);
        arrayInputField.add(track_shoe_pn);
        arrayInputField.add(master_shoe_pn);
        arrayInputField.add(carrier_roller_pn);
        arrayInputField.add(front_idler_pn);
        arrayInputField.add(rear_idler_pn);
        arrayInputField.add(single_flange_pn);
        arrayInputField.add(double_flange_pn);
        arrayInputField.add(first_sprocket_segment_pn);
        arrayInputField.add(seconds_sprocket_segment_pn);

        presenter = new ReportFragmentGeneralPresenter(this, arrayInputField);
        pickImage = new PickImageHelper(getContext());
        permission = new PermissionHelper(getActivity());

        presenter.setSpinnerData(family, R.array.family);
        presenter.setSpinnerData(underfoot_impact, R.array.underfoot);
        presenter.setSpinnerData(underfoot_abrasive, R.array.underfoot);
        presenter.setSpinnerData(underfoot_moisture, R.array.underfoot);
        presenter.setSpinnerData(underfoot_packing, R.array.underfoot);
        presenter.setSpinnerData(bushing_allowable_wear, R.array.allowable_wear);
        presenter.setSpinnerData(link_allowable_wear, R.array.allowable_wear);
        presenter.setSpinnerData(bushings_turned, R.array.option);
        presenter.setSpinnerData(shoe_type, R.array.shoe_type);
        presenter.setSpinnerData(unit_of_measure, R.array.unit_of_measure);
        presenter.setDate(inspection_date);
        presenter.pickLocationCoordinate(gps_coordinate);
    }

    @OnClick(R.id.gps_coordinate) void pickCoordinate() {
        presenter.startResult(true);
    }

    @OnClick(R.id.next) void clickNext() {
        if (TextUtils.isEmpty(mCurrentPhotoPath)) {
            loader.hideProgressDialog();
            DialogShowMessages.getInstance(getContext()).setMessage("Please input image!").show();
        } else if (presenter.validate()) {
            loader.showProgressDialog();
            gotToLeft();
        }
    }

    private void gotToLeft() {
        new Handler().postDelayed(() -> loader.hideProgressDialog(), 2700);
        presenter.saveData(arraySpinner, arrayInputField);
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_report, new ReportFragmentLeft())
                .addToBackStack(null)
                .commit();
    }

    @OnClick(R.id.photo) void choosePhoto() {
        mCurrentPhotoPath = null;
        DialogPickImage.getInstance(getContext())
                .setCallback(new DialogPickImage.ClickDialog() {
                    @Override public void clickCamera() {
                        pickImageFromCamera(REQUEST_CAMERA);
                    }

                    @Override public void clickGallery() {
                        pickImageFromGallery(REQUEST_GALLERY);
                    }
                }).show();
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                gotToHome();
                break;
            case R.id.menu_next:
                clickNext();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void pickImageFromGallery(int REQUEST_CODE) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CODE);
    }

    private static final String TAG = "ReportFragmentGeneral";

    public void pickImageFromCamera(int REQUEST_CODE) {
        if (permission.isSdkVersionM()) {
            reportActivityView.requestPermissionCamera(() -> startOpenCamera(REQUEST_CODE));
        } else if (permission.isCameraPermissionGranted()) {
            startOpenCamera(REQUEST_CODE);
        } else {
            startOpenCamera(REQUEST_CODE);
        }
    }

    private void startOpenCamera(int REQUEST_CODE) {
        Log.d(TAG, "startOpenCamera: camera opened!");
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = pickImage.createImageFile(REQUEST_CODE);
            if (photoFile != null) {
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, REQUEST_CODE);
            }
        }
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    pickImage.setImgFromCamera(photo, path -> {
                        mCurrentPhotoPath = path;
                        SharedPref.setDefaults(IMAGE_FRAGMENT_GENERAL, mCurrentPhotoPath, getContext());
                    });
                    break;
                case REQUEST_GALLERY:
                    pickImage.setImgFromGallery(data, photo, path -> {
                        mCurrentPhotoPath = path;
                        SharedPref.setDefaults(IMAGE_FRAGMENT_GENERAL, mCurrentPhotoPath, getContext());
                    });
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override public void onStart() {
        SetToolbar.setTitleFragment(this, "GENERAL");
        if (reportActivityView == null)
            reportActivityView = (ReportActivityView) getActivity();
        String currentLocation = SharedPref.getDefaults(Constants.Maps.CURRENT_LAT_LON, getContext());
        String lastLocation = SharedPref.getDefaults(Constants.Maps.LAST_LAT_LON, getContext());
        if (currentLocation != null) {
            gps_coordinate.setText(currentLocation);
        } else if (lastLocation != null) {
            gps_coordinate.setText(lastLocation);
        }
        super.onStart();
    }

    @Override public void onResume() {
        setupPresenter();
        if (reportActivityView != null) {
            reportActivityView.setTitleToolbar("GENERAL");
            presenter.setOnResumeData(arraySpinner, arrayInputField);

            String cacheImage = SharedPref.getDefaults(IMAGE_FRAGMENT_GENERAL, getContext());
            if (cacheImage != null) {
                pickImage.setImageFromPath(photo, cacheImage);
                mCurrentPhotoPath = cacheImage;
            }
        }
        super.onResume();
    }

    private void gotToHome() {
        Intent intent = new Intent(getContext(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override public void onStop() {
        reportActivityView = null;
        super.onStop();
    }

}
