package com.ahmadrosid.inspections.fragment_view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.fragment_view.presenter.home.FragmentListPresenter;
import com.ahmadrosid.inspections.helper.SetToolbar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ocittwo on 31/08/16.
 */
public class FragmentList extends Fragment{

    private FragmentListPresenter presenter;

    @BindView(R.id.item_list_list) RecyclerView item_list_list;

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SetToolbar.setTitleFragment(this, "List");
    }

    @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupPresenter();
    }

    private void setupPresenter() {
        presenter = new FragmentListPresenter(this);
        presenter.setListList(item_list_list);

    }
}
