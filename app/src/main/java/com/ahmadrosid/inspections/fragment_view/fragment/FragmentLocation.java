package com.ahmadrosid.inspections.fragment_view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.activity.PickLocation;
import com.ahmadrosid.inspections.helper.Constants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ocittwo.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 * <p>
 * © 2016 | All Rights Reserved
 */
public class FragmentLocation extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;


    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_locations, container, false);
        ButterKnife.bind(this, view);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return view;
    }

    @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupView();
    }

    private void setupView() {

    }

    @Override public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng br_location = new LatLng(-7.784180, 110.335170);
        mMap.addMarker(new MarkerOptions().position(br_location).title("Marker my location!"));
        CameraPosition myPosition = new CameraPosition.Builder().target(br_location).zoom(15).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(myPosition));
    }

    @OnClick(R.id.btn_set_location) void clickSetLocation() {
        Intent intent = new Intent(getContext(), PickLocation.class);
        startActivityForResult(intent, Constants.REQUEST_SET_MY_LOCATION);
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constants.REQUEST_SET_MY_LOCATION:
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
