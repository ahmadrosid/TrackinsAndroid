package com.ahmadrosid.inspections.fragment_view.presenter.report;

import android.content.Context;
import android.text.TextUtils;

import com.ahmadrosid.inspections.core.Presenter;
import com.ahmadrosid.inspections.data.local.TrackinsCacheManager;
import com.ahmadrosid.inspections.fragment_view.fragment.report.ReportFragmentLeft;
import com.ahmadrosid.inspections.fragment_view.view.report.FragmentLeftRightReportView;
import com.ahmadrosid.inspections.helper.Constants;
import com.ahmadrosid.inspections.helper.PickImageHelper;
import com.ahmadrosid.inspections.ui.FormInputImage;
import com.ahmadrosid.inspections.ui.dialog.DialogPickImage;
import com.ahmadrosid.inspections.ui.dialog.DialogShowMessages;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.ahmadrosid.inspections.helper.Constants.REQUEST_CAMERA;
import static com.ahmadrosid.inspections.helper.Constants.REQUEST_GALLERY;

/**
 * Created by ocittwo on 10/9/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class ReportFragmentLeftPresenter implements Presenter<FragmentLeftRightReportView> {
    private Context context;
    private FragmentLeftRightReportView view;
    private ArrayList<FormInputImage> arrayForm;
    private TrackinsCacheManager cacheManager;

    public ReportFragmentLeftPresenter(ReportFragmentLeft fragmentLeftReport, ArrayList<FormInputImage> arrayForm) {
        this.context = fragmentLeftReport.getContext();
        this.arrayForm = arrayForm;
        cacheManager = new TrackinsCacheManager(fragmentLeftReport.getContext());
    }

    public void setPickImage() {
        for (FormInputImage formInputImage : arrayForm) {
            formInputImage.getImageView().setOnClickListener(v -> pickImageFrom(formInputImage));
        }
    }

    private void pickImageFrom(FormInputImage formInputImage) {
        view.setCurrentForm(formInputImage);
        DialogPickImage.getInstance(context)
                .setCallback(new DialogPickImage.ClickDialog() {
                    @Override public void clickCamera() {
                        view.pickImageFromCamera(REQUEST_CAMERA);
                    }

                    @Override public void clickGallery() {
                        view.pickImageFromGallery(REQUEST_GALLERY);
                    }
                }).show();
    }

    public boolean nextToRight() {
        return validate();
    }

    private boolean validate() {
        boolean isValidate = true;
        boolean isShowEmpty = false;
        for (FormInputImage formInputImage : arrayForm) {
            if (formInputImage.isRequired()) {
                if (TextUtils.isEmpty(formInputImage.getText())) {
                    isValidate = false;
                    if (!isShowEmpty) {
                        showEmptyField(formInputImage.getHint());
                        isShowEmpty = true;
                    }
                }
            }
        }
        return isValidate;
    }

    private void showEmptyField(String s) {
        DialogShowMessages.getInstance(context).setMessage("Please input " + s).show();
    }

    public void resumeFragment(ArrayList<FormInputImage> arrayForm) {
        Map<Object, List<String>> listMap = cacheManager.getCacheFragmentReportLeft();
        if (listMap != null) {
            List<String> dataField = listMap.get(Constants.JSON_LEFT_INPUT_FIELD);
            List<String> dataHint = listMap.get(Constants.JSON_LEFT_TEXT_HINT);
            List<String> dataImage = listMap.get(Constants.JSON_LEFT_IMAGE_PATH);
            for (int i = 0; i < arrayForm.size(); i++) {
                FormInputImage formInputImage = arrayForm.get(i);
                if (dataField.get(i) != null) formInputImage.setText(dataField.get(i));
                if (dataHint.get(i) != null) formInputImage.setHint(dataHint.get(i));
                String pathImage = dataImage.get(i);
                if (pathImage != null) {
                    formInputImage.setImgPath(pathImage);
                    new PickImageHelper(context).setImageFromPath(formInputImage.getImageView(), pathImage);
                }
            }
        }
    }

    private static final String TAG = "FragmentReportLeftPrese";

    public void cache(ArrayList<FormInputImage> arrayForm) {
        cacheManager.cacheFragmentReportLeft(arrayForm);
    }

    @Override public void onAttachView(FragmentLeftRightReportView view) {
        this.view = view;
    }

    @Override public void onDetachView() {
        view = null;
    }
}
