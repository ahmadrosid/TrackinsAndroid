package com.ahmadrosid.inspections.helper;

import android.util.Log;

/**
 * Created by ocittwo on 31/08/16.
 */
public class Logging {
    private static final String TAG = "Logging";

    public static void log(String s){
        Log.d(TAG, "log: " + s);
    }

    public static void log(Object o, String s){
        Log.d(o.getClass().getName(), "log: " + s);
    }

    public static void logw(String s, Throwable e){
        Log.w(TAG, "logw: " + s, e);
    }
}
