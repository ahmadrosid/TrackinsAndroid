package com.ahmadrosid.inspections.helper;

import android.content.Context;

import com.ahmadrosid.inspections.api.ApiBuilder;
import com.ahmadrosid.inspections.api.ApiServices;
import com.ahmadrosid.inspections.api.model.ResponseRequest;
import com.ahmadrosid.inspections.data.model.DBModelInspections;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ocittwo on 04/09/16.
 */
public class UploadHelper {

    /**
     * Post data to server
     *
     * @param context Context
     * @param data_local DBModelInspections
     * @param file File
     */
    public static void postData(final Context context, final DBModelInspections data_local, File file){

        /**
         * Create attribute request body images
         */
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body_pictures = MultipartBody.Part.createFormData("pictures", file.getName(), requestFile);

        /**
         * Attribute request body
         */
        RequestBody body_title = createRequestBody(data_local.title);
        RequestBody body_company = createRequestBody(data_local.company);
        RequestBody body_descriptions = createRequestBody(data_local.description);

        /**
         * Executed store to server
         */
        ApiServices services = ApiBuilder.call();
        services.create(body_title, body_company, body_descriptions, body_pictures)
                .enqueue(new Callback<ResponseRequest>() {
                    /**
                     * When have response
                     *
                     * @param call Call
                     * @param response ResponseBody
                     */
                    @Override public void onResponse(Call<ResponseRequest> call, Response<ResponseRequest> response) {
                        ResponseRequest request = response.body();
                        NotifHelper.hideNotification(context);
                        if (request.isSuccess()) {
                            Logging.log("OnResponse: Post Success and response success!");
                        }
                    }

                    /**
                     * Catch error of storing database
                     *
                     * @param call Call
                     * @param t Throwable
                     */
                    @Override public void onFailure(Call<ResponseRequest> call, Throwable t) {
                        t.printStackTrace();
                        NotifHelper.commonNotif(context, "Upload data to server", "Failed upload data");
                        Logging.logw("OnFailure", t);
                    }
                });
    }

    /**
     * Create request body for input
     *
     * @param v String
     * @return RequestBody
     */
    private static RequestBody createRequestBody(String v) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), v);
    }
}
