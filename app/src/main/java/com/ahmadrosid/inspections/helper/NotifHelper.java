package com.ahmadrosid.inspections.helper;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.ahmadrosid.inspections.R;

/**
 * Created by ocittwo on 03/09/16.
 */
public class NotifHelper {
    private static final String TAG = "NotifHelper";

    /**
     * Show notif upload to server
     *
     * @param context Context
     */
    public static void showNotifUpload(final Context context) {
        Logging.log("Notification have been executed!");
        final NotificationManager mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        final NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle("Uploading data to server")
                        .setContentText("Please wait a minute!");

        /**
         * SetToolbar progress upload
         */
        new Thread(() -> {
            int incr;
            for (incr = 0; incr <= 100; incr += 15) {
                if (NetworkHelper.isNetworkConnected(context)) {
                    mBuilder.setProgress(100, incr, false);
                    mNotificationManager.notify(1, mBuilder.build());
                    try {
                        Thread.sleep(5 * 1000);
                    } catch (InterruptedException e) {
                        Log.d(TAG, "sleep failure");
                    }
                }else{
                    mBuilder.setContentText("Upload Failed!")
                            .setProgress(0, 0, false);
                    mNotificationManager.notify(1, mBuilder.build());
                }
            }
            mBuilder.setContentText("Upload complete!")
                    .setProgress(0, 0, false);
            mNotificationManager.notify(1, mBuilder.build());
            hideNotification(context);
        }).start();

    }

    /**
     * Hide notification upload
     *
     * @param context Context
     */
    static void hideNotification(Context context) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) context.getSystemService(ns);
        nMgr.cancel(1);
    }

    public static void commonNotif(final Context context, String title, String desc){
        final NotificationManager mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        final NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle(title)
                        .setContentText(desc);
        mNotificationManager.notify(2, mBuilder.build());
        new Handler().postDelayed(() -> hideNotification(context), 5000);
    }
}
