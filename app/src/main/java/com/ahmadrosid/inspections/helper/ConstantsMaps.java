package com.ahmadrosid.inspections.helper;

/**
 * Created by ocittwo on 10/4/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */

public class ConstantsMaps {
    public static final String RECEIVER = "receiver";
    public static final String LOCATION_DATA_EXTRA = "location data extra";
    public static final int FAILURE_RESULT = 0;
    public static final int SUCCESS_RESULT = 1;
    public static final String RESULT_DATA_KEY = "result data key";
}
