package com.ahmadrosid.inspections.helper.constants;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ocittwo on 10/6/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class DatabaseConstants {

    public static final String TABLE_DATA_SETTING_LOCAL = "data_settings_local";

    public static final String ORGANIZATION = "organization";
    public static final String REGION = "region";
    public static final String DISTRICT = "district";
    public static final String DEALER_NAME = "dealer_name";
    public static final String DEALER_CODE = "dealer_code";
    public static final String SALES_NAME = "sales_name";
    public static final String CREATED_AT = "created_at";

    @SuppressLint("SimpleDateFormat") public static String getCreatedAt(){
        return new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
    }
}
