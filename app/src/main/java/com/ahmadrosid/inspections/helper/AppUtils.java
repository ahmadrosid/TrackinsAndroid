package com.ahmadrosid.inspections.helper;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import static android.provider.Settings.Secure.LOCATION_MODE;

/**
 * Created by ocittwo on 10/4/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class AppUtils {

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), LOCATION_MODE);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public class LocationConstants {
        public static final String RESULT_DATA_KEY = "result data key";
        public static final String LOCATION_DATA_AREA = "location data area";
        public static final String LOCATION_DATA_CITY = "location data city";
        public static final String LOCATION_DATA_STREET = "location data street";
        public static final int SUCCESS_RESULT = 1;
    }
}
