package com.ahmadrosid.inspections.helper;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.zelory.compressor.FileUtil;

/**
 * Created by ocittwo on 10/8/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */

public class PickImageHelper {
    private static final String TAG = "PickImageHelper";

    private final Context context;
    private String mCurrentPhotoPath;

    public PickImageHelper(Context context) {
        this.context = context;
    }

    public File createImageFile(int requestCode) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,
                    ".jpeg",
                    storageDir
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert image != null;
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    public void setImgFromGallery(Intent data, ImageView img, PickImageHelperImpl result) {
        Uri selectedImageUriLeft = data.getData();
        try {
            File file = FileUtil.from(context, selectedImageUriLeft);
            File img_compress = BitmapHelper.compressImage(context, file);
            setImageFromPath(img, "file:" + img_compress.getPath());
            result.resultImagePath("file:" + img_compress.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setImgFromCamera(ImageView imageView, PickImageHelperImpl result) {
        setImageFromPath(imageView, mCurrentPhotoPath);
        result.resultImagePath(mCurrentPhotoPath);
    }

    public void setImageFromPath(ImageView imageView, String path) {
        if (context != null) {
            try {
                Bitmap mImageBitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.parse(path));
                imageView.setImageBitmap(mImageBitmap);
                styleImage(imageView);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public interface PickImageHelperImpl {
        void resultImagePath(String path);
    }

    private void styleImage(ImageView imageView) {
        imageView.setPadding(0, 0, 0, 0);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }
}
