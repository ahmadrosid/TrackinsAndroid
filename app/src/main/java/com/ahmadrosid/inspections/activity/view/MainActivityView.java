package com.ahmadrosid.inspections.activity.view;

import android.support.v4.app.Fragment;

/**
 * Created by ocittwo on 31/08/16.
 */
public interface MainActivityView {

    /**
     *
     * @param fragment
     */
    void setActiveFragment(Fragment fragment);
}
