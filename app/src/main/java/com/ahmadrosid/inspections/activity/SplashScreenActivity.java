package com.ahmadrosid.inspections.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.helper.SharedPref;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ahmadrosid.inspections.helper.Constants.API_TOKEN;
import static com.ahmadrosid.inspections.helper.Constants.HAVE_SETTINGS;

/**
 * Created by ocittwo on 02/10/16.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 * <p>
 * © 2016 | App All Rights Reserved
 */
public class SplashScreenActivity extends AppCompatActivity {

    @BindView(R.id.start_loading) LinearLayout start_loading;

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        setLoading(100, 0, start_loading);
    }

    private void setLoading(final int percent, final int position, final LinearLayout view) {
        final LinearLayout.LayoutParams percent_start = (LinearLayout.LayoutParams) start_loading.getLayoutParams();
        final int index = position + 2;
        new Handler().postDelayed(() -> {
            if (position < percent) {
                percent_start.weight = index;
                view.setLayoutParams(percent_start);
                setLoading(percent, index, view);
            } else {
                checkLogin();
            }
        }, 50);
    }

    private void checkLogin() {
        String token = SharedPref.getDefaults(API_TOKEN, getApplicationContext());
        if (token == null) {
            goTo(LoginActivity.class);
        } else {
            String have_settings = SharedPref.getDefaults(HAVE_SETTINGS, getApplicationContext());
            if (have_settings == null) {
                goTo(SettingsActivity.class);
            } else {
                goTo(MainActivity.class);
            }
        }
    }

    private void goTo(Class clazz) {
        Intent i = new Intent(getApplicationContext(), clazz);
        startActivity(i);
        finish();
    }
}
