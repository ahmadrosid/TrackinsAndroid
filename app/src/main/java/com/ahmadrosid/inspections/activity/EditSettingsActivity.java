package com.ahmadrosid.inspections.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.ui.ButtonPrimary;
import com.ahmadrosid.inspections.ui.FloatLabelTextView;

import butterknife.BindView;

/**
 * Created by ocittwo on 10/14/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */

public class EditSettingsActivity extends AppCompatActivity{

    @BindView(R.id.input_organization) FloatLabelTextView organization;
    @BindView(R.id.input_region) FloatLabelTextView region;
    @BindView(R.id.input_district) FloatLabelTextView district;
    @BindView(R.id.input_dealer_name) FloatLabelTextView dealer_name;
    @BindView(R.id.input_dealer_code) FloatLabelTextView dealer_code;
    @BindView(R.id.input_sales_name) FloatLabelTextView sales_name;
    @BindView(R.id.btn_save_settings) ButtonPrimary btn_save_settings;

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }
}
