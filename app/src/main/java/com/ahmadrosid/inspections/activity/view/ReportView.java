package com.ahmadrosid.inspections.activity.view;

import android.support.v4.app.Fragment;

/**
 * Created by ocittwo on 28/09/16.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 *
 * © 2016 | App All Rights Reserved
 */
public interface ReportView {
    void setActiveFragment(Fragment fragment);
}
