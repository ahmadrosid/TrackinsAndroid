package com.ahmadrosid.inspections.activity.presenter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import com.ahmadrosid.inspections.activity.view.CreateReportView;
import com.ahmadrosid.inspections.data.Database;
import com.ahmadrosid.inspections.data.model.DBModelInspections;
import com.ahmadrosid.inspections.helper.BitmapHelper;
import com.ahmadrosid.inspections.helper.NetworkHelper;
import com.ahmadrosid.inspections.helper.UploadHelper;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.zelory.compressor.FileUtil;

/**
 * Created by ocittwo on 01/09/16.
 * <p/>
 * Handle data managing of report wich is will store on server and local dtabase
 */
public class CreateReportPresenter {
    private static final String TAG = "CreateReportPresenter";

    private final CreateReportView reportView;
    private final Context context;

    /**
     * Instance object presenter
     *
     * @param context    Context
     * @param reportView View of createUserProfile report
     */
    public CreateReportPresenter(Context context, CreateReportView reportView) {
        this.context = context;
        this.reportView = reportView;
    }

    /**
     * helper to retrieve the path of an image URI
     */
    public String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }

        return uri.getPath();
    }

    /**
     * Save data report if have internet connection store to server if not store on local database
     *
     * @param selectedImagePath Path images
     * @param title             Title Report
     * @param company           CompanyName
     * @param descriptions      Description report
     */
    public void saveReport(String selectedImagePath, String title, String company, String descriptions) {
        if (isValidate(selectedImagePath, title, company, descriptions)) {
            saveData(selectedImagePath, title, company, descriptions);
        }
    }

    /**
     * Save report into local sqlite database
     *
     * @param selectedImagePath Path images
     * @param title             Title Report
     * @param company           CompanyName
     * @param descriptions      Description report
     */
    private void saveData(String selectedImagePath, String title, String company, String descriptions) {
        Uri uri = Uri.parse("file://" + selectedImagePath);
        try {
            /**
             * Compress a file
             */
            File file = FileUtil.from(context, uri);
            File img_compress = BitmapHelper.compressImage(context, file);

            /**
             * Create object data inspections
             * Data wil fetch after BitmapHelper.saveBitmap() executed
             */
            String created_at = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
            String img_path = img_compress.getAbsolutePath();


            /**
             * Save to database
             */
            Database db = new Database(context);
            SQLiteDatabase database = db.getWritableDatabase();
            if (NetworkHelper.isNetworkConnected(context)) {
                DBModelInspections data = new DBModelInspections(
                        title, selectedImagePath, company, descriptions, "0", img_path, created_at
                );
                db.insert(database, data);
                UploadHelper.postData(context, data, BitmapHelper.compressImage(context, file));
                reportView.showUnvalidate("Database have been saved on server!");
                reportView.createReportSuccess(true);
            } else {
                DBModelInspections data = new DBModelInspections(
                        title, selectedImagePath, company, descriptions, "1", img_path, created_at
                );
                db.insert(database, data);
                reportView.showUnvalidate("Database have been saved on SQLite database!");
                reportView.createReportSuccess(false);
            }

        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "uploadReportToServer: failed" + e.toString());
        }


    }

    /**
     * Validating form input report
     *
     * @param selectedImagePath Path images
     * @param title             Title Report
     * @param company           CompanyName
     * @param description       Description report
     * @return boolean
     */
    private boolean isValidate(String selectedImagePath, String title, String company, String description) {
        if (isEmpty(selectedImagePath)) {
            reportView.showUnvalidate("Please input image!");
            return false;
        } else if (isEmpty(title)) {
            reportView.showUnvalidate("Please input Title!");
            return false;
        } else if (isEmpty(company)) {
            reportView.showUnvalidate("Please Input company!");
            return false;
        } else if (isEmpty(description)) {
            reportView.showUnvalidate("Please input Descriptions!");
            return false;
        } else {
            return true;
        }
    }

    /**
     * Cek empty text
     *
     * @param s Strings
     * @return boolean
     */
    public boolean isEmpty(String s) {
        return TextUtils.isEmpty(s);
    }

}
