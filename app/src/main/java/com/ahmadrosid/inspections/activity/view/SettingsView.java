package com.ahmadrosid.inspections.activity.view;

import android.content.Context;

import com.ahmadrosid.inspections.core.Presenter;
import com.ahmadrosid.inspections.data.DataSettings;

/**
 * Created by ocittwo on 10/6/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public interface SettingsView extends Presenter.View{
    boolean validateForm();
    Context getContext();
    void showLoader();
    void hideLoader();
    void successSaveSettings();
}
