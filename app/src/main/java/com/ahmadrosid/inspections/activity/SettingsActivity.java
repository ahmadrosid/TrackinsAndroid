package com.ahmadrosid.inspections.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.activity.presenter.SettingsPresenter;
import com.ahmadrosid.inspections.activity.view.SettingsView;
import com.ahmadrosid.inspections.data.DataSettings;
import com.ahmadrosid.inspections.helper.Constants;
import com.ahmadrosid.inspections.helper.ProgressLoader;
import com.ahmadrosid.inspections.helper.SetToolbar;
import com.ahmadrosid.inspections.helper.SharedPref;
import com.ahmadrosid.inspections.ui.ButtonPrimary;
import com.ahmadrosid.inspections.ui.FloatLabelTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ocittwo on 10/6/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class SettingsActivity extends AppCompatActivity implements SettingsView {

    @BindView(R.id.input_organization) FloatLabelTextView organization;
    @BindView(R.id.input_region) FloatLabelTextView region;
    @BindView(R.id.input_district) FloatLabelTextView district;
    @BindView(R.id.input_dealer_name) FloatLabelTextView dealer_name;
    @BindView(R.id.input_dealer_code) FloatLabelTextView dealer_code;
    @BindView(R.id.input_sales_name) FloatLabelTextView sales_name;
    @BindView(R.id.btn_save_settings) ButtonPrimary btn_save_settings;

    private ProgressLoader loader;
    private SettingsPresenter presenter;

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        SetToolbar.get(this).setTitle("Settings");
        ButterKnife.bind(this);

        presenter = new SettingsPresenter(this);
        loader = new ProgressLoader(getContext());

        boolean isEditMode = getIntent().getBooleanExtra(Constants.Settings.IS_EDIT_MODE, false);
        if (isEditMode){
            setEditModel();
        }
    }

    private void setEditModel() {
        DataSettings data = DataSettings.getInstance().get();
        organization.setText(data.organization);
        region.setText(data.region);
        district.setText(data.district);
        dealer_name.setText(data.dealer_name);
        dealer_code.setText(data.dealer_code);
        sales_name.setText(data.sales_name);
    }

    @Override public boolean validateForm() {
        boolean isValidate = false;
        if (check(organization.getText().toString())){
            showSnack("Please input Organization!");
        }else if (check(region.getText().toString())) {
            showSnack("Please input Region!");
        }else if (check(district.getText().toString())){
            showSnack("Please input District");
        }else if (check(dealer_name.getText().toString())){
            showSnack("Please input Dealer Name!");
        }else if (check(dealer_code.getText().toString())){
            showSnack("Please input Dealer Code!");
        }else if (check(sales_name.getText().toString())){
            showSnack("Please input Sales Name");
        }else{
            DataSettings.getInstance()
                    .setOrganization(organization.getText().toString())
                    .setRegion(region.getText().toString())
                    .setDistrict(district.getText().toString())
                    .setDealer_name(dealer_name.getText().toString())
                    .setDealer_code(dealer_code.getText().toString())
                    .setSales_name(sales_name.getText().toString())
                    .save();
            isValidate = true;
        }
        return isValidate;
    }

    @Override public Context getContext() {
        return this;
    }

    @Override public void showLoader() {
        loader.showProgressDialog();
    }

    @Override public void hideLoader() {
        loader.hideProgressDialog();
    }

    @Override public void successSaveSettings() {
        SharedPref.setDefaults("api_token", "993ee6ahddkajmhd7m3794ee3e3e3", getContext());
        Intent i = new Intent(getContext(), MainActivity.class);
        startActivity(i);
        finish();
    }

    @OnClick(R.id.btn_save_settings) void clickSaveSetting(){
        presenter.saveSettings();
    }

    private boolean check(String o){
        return TextUtils.isEmpty(o);
    }

    private void showSnack(String s){
        Snackbar.make(btn_save_settings, s, Snackbar.LENGTH_SHORT).show();
    }

    @Override protected void onStart() {
        presenter.onAttachView(this);
        super.onStart();
    }

    @Override protected void onStop() {
        presenter.onDetachView();
        super.onStop();
    }
}
