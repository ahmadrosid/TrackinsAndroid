package com.ahmadrosid.inspections.core;

/**
 * Created by ocittwo on 10/13/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public interface Presenter<T extends Presenter.View> {

    void onAttachView(T view);

    void onDetachView();

    interface View{}
}
