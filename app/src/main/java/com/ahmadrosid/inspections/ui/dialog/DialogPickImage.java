package com.ahmadrosid.inspections.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import com.ahmadrosid.inspections.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ocittwo on 10/8/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */

public class DialogPickImage extends Dialog{

    private ClickDialog clickDialog;

    public static DialogPickImage instance;

    public static DialogPickImage getInstance(Context context) {
        instance = new DialogPickImage(context);
        return instance;
    }

    public DialogPickImage setCallback(ClickDialog clickDialog){
        this.clickDialog = clickDialog;
        return this;
    }

    public DialogPickImage(Context context) {
        super(context);
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_pick_image);
        getWindow().getDecorView();
        ButterKnife.bind(this);
    }


    @OnClick(R.id.camera) void selectCamera(){
        clickDialog.clickCamera();
        dismiss();
    }

    @OnClick(R.id.gallery) void selectGallery(){
        clickDialog.clickGallery();
        dismiss();
    }

    public interface ClickDialog{
        void clickCamera();
        void clickGallery();
    }

}
