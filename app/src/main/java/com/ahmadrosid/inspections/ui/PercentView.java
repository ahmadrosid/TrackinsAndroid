package com.ahmadrosid.inspections.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ahmadrosid.inspections.R;

/**
 * Created by ocittwo on 02/10/16.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 *
 * © 2016 | App All Rights Reserved
 */
public class PercentView extends LinearLayout {

    private TextView mHintView;
    private TextView mTextPercent;
    private EditText mEditText;

    private LinearLayout mWrapPercent;
    private LinearLayout mStartPercent;
    private LinearLayout mEndPercent;

    public PercentView(Context context) {
        this(context, null);
    }

    public PercentView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.START);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.percent_view, this, true);

        TypedArray array = context.obtainStyledAttributes(attrs,
                R.styleable.PercentView, 0, 0);
        String hint = array.getString(R.styleable.PercentView_hint);

        array.recycle();

        mEditText = (EditText) findViewById(R.id.input_text);
        mHintView = (TextView) findViewById(R.id.text_hint);
        mTextPercent = (TextView) findViewById(R.id.text_percent);

        mWrapPercent = (LinearLayout) findViewById(R.id.wrap_percent);
        mStartPercent = (LinearLayout) findViewById(R.id.start_percent);
        mEndPercent = (LinearLayout) findViewById(R.id.end_percent);

        mEditText.setHint(hint);
        mHintView.setText(hint);

        Typeface lato_light = Typeface.createFromAsset(context.getAssets(),"fonts/Lato-Light.ttf");
        mEditText.setTypeface(lato_light);
        mHintView.setTypeface(lato_light);

        mEditText.addTextChangedListener(new TextWatcher() {
            @Override public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    showPercent(mEditText.getText().toString());
                } else {
                    showPercent("0");
                }
            }

            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });
    }

    private void showPercent(String s) {
        int percent = Integer.parseInt(s);
        if (percent == 0) {
            mWrapPercent.setVisibility(GONE);
        } else {
            setPercent(percent, 0, mStartPercent);

            LinearLayout.LayoutParams percent_end = (LayoutParams) mEndPercent.getLayoutParams();
            percent_end.weight = 100;
            mEndPercent.setLayoutParams(percent_end);

            mWrapPercent.setVisibility(VISIBLE);
            mHintView.setVisibility(VISIBLE);
            mTextPercent.setVisibility(VISIBLE);
        }
    }

    public void setText(String s) {
        showPercent(s);
    }

    private void setPercent(final int percent, final int position, final LinearLayout view) {
        final LinearLayout.LayoutParams percent_start = (LayoutParams) mStartPercent.getLayoutParams();
        final int index = position + 1;
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                if (position < percent) {
                    percent_start.weight = index;
                    view.setLayoutParams(percent_start);
                    mTextPercent.setText("" + index + " %");
                    setPercent(percent, index, view);
                }
            }
        }, 1);
    }
}
