package com.ahmadrosid.inspections.ui.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.widget.TextView;

import com.ahmadrosid.inspections.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ocittwo on 10/9/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */

public class DialogShowMessages extends Dialog {

    private String text_message;

    @BindView(R.id.message) TextView message;

    @SuppressLint("StaticFieldLeak") private static DialogShowMessages instance;

    public static DialogShowMessages getInstance(Context context) {
        instance = new DialogShowMessages(context);
        return instance;
    }

    public DialogShowMessages setMessage(String text_message) {
        this.text_message = text_message;
        return this;
    }

    public DialogShowMessages setDismis(int time) {
        new Handler().postDelayed(this::dismiss, time);
        return this;
    }

    public DialogShowMessages(Context context) {
        super(context);
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_show_messages);
        getWindow().getDecorView();
        ButterKnife.bind(this);
        setupView();
    }

    private void setupView() {
        if (message != null)
            message.setText(text_message);
    }

    @OnClick(R.id.text_ok) void clickOk() {
        dismiss();
    }
}
