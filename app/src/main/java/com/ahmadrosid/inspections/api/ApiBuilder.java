package com.ahmadrosid.inspections.api;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ocittwo.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 *
 * © 2016 | All Rights Reserved
 */
public class ApiBuilder {

    /* Productions Server*/
//    private static final String API_BASE_URL = "http://76.164.195.170:4000/";

    /* Local Server */
    public static final String API_BASE_URL = "http://192.168.43.17/";

    public static ApiServices call() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        return retrofit.create(ApiServices.class);
    }

}
