package com.ahmadrosid.inspections.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by ocittwo on 10/4/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class HolderNotification extends RecyclerView.ViewHolder{
    public HolderNotification(View itemView) {
        super(itemView);
    }

    public void bind() {

    }
}
