package com.ahmadrosid.inspections.data.local;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ahmadrosid.inspections.Trackins;
import com.ahmadrosid.inspections.data.model.TrackinsSetting;

/**
 * Created by ocittwo on 10/7/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public enum TrackinsDatabaseHelper {
    INSTANCE;

    private final SQLiteDatabase sqLiteDatabase;

    TrackinsDatabaseHelper() {
        TrackinsDbOpenHelper qiscusDbOpenHelper = new TrackinsDbOpenHelper(Trackins.getAppInstance());
        sqLiteDatabase = qiscusDbOpenHelper.getReadableDatabase();
    }

    public static TrackinsDatabaseHelper getInstance() {
        return INSTANCE;
    }

    public void add(TrackinsSetting trackinsSetting) {
        if (!isContains(trackinsSetting)) {
            sqLiteDatabase.beginTransaction();
            try {
                sqLiteDatabase.insert(TrackinsDB.SettingsTable.TABLE_NAME, null, TrackinsDB.SettingsTable.toContentValues(trackinsSetting));
                sqLiteDatabase.setTransactionSuccessful();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                sqLiteDatabase.endTransaction();
            }
        }
    }

    public boolean isContains(TrackinsSetting trackinsSetting) {
        String query;
        if (trackinsSetting.getId() == -1) {
            query = "SELECT * FROM "
                    + TrackinsDB.SettingsTable.TABLE_NAME + " WHERE "
                    + TrackinsDB.SettingsTable.COLUMN_UNIQUE_ID + " = '" + trackinsSetting.getUnique_id() + "'";
        } else {
            query = "SELECT * FROM "
                    + TrackinsDB.SettingsTable.TABLE_NAME + " WHERE "
                    + TrackinsDB.SettingsTable.COLUMN_ID + " = " + trackinsSetting.getId() + " OR "
                    + TrackinsDB.SettingsTable.COLUMN_UNIQUE_ID + " = '" + trackinsSetting.getUnique_id() + "'";
        }
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        boolean contains = cursor.getCount() > 0;
        cursor.close();
        return contains;
    }

}
