package com.ahmadrosid.inspections.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ocittwo on 10/7/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class TrackinsReportGeneral implements Parcelable{

    public long id;
    public String unique_id;
    public String customer_name;
    public String customer_number;
    public String job_site;
    public String gps_coordinate;
    public String photo_path;
    public String family;
    public String model_config;
    public String model;
    public String equipment_number;
    public String inspections_date;
    public String hour_meter_reading;
    public String hour_meter_per_week;
    public String underfoot_impact;
    public String underfoot_abrasive;
    public String underfoot_moisture;
    public String underfoot_packing;
    public String bushing_allowable_wear;
    public String link_allowable_wear;
    public String bushings_turned;
    public String notes;
    public String shoe_type;
    public String unit_of_measure;
    public String track_roller_quantities_sf;
    public String track_roller_quantities_df;
    public String track_group_pn;
    public String link_assembly_pn;
    public String track_shoe_pn;
    public String master_shoe_pn;
    public String carrier_roller_pn;
    public String front_idler_pn;
    public String rear_idler_pn;
    public String single_flange_pn;
    public String double_flange_pn;
    public String first_sprocket_segment_pn;
    public String seconds_sprocket_segment_pn;
    public String created_at;

    public TrackinsReportGeneral() {
    }

    protected TrackinsReportGeneral(Parcel in) {
        id = in.readLong();
        unique_id = in.readString();
        customer_name = in.readString();
        customer_number = in.readString();
        job_site = in.readString();
        gps_coordinate = in.readString();
        photo_path = in.readString();
        family = in.readString();
        model_config = in.readString();
        model = in.readString();
        equipment_number = in.readString();
        inspections_date = in.readString();
        hour_meter_reading = in.readString();
        hour_meter_per_week = in.readString();
        underfoot_impact = in.readString();
        underfoot_abrasive = in.readString();
        underfoot_moisture = in.readString();
        underfoot_packing = in.readString();
        bushing_allowable_wear = in.readString();
        link_allowable_wear = in.readString();
        bushings_turned = in.readString();
        notes = in.readString();
        shoe_type = in.readString();
        unit_of_measure = in.readString();
        track_roller_quantities_sf = in.readString();
        track_roller_quantities_df = in.readString();
        track_group_pn = in.readString();
        link_assembly_pn = in.readString();
        track_shoe_pn = in.readString();
        master_shoe_pn = in.readString();
        carrier_roller_pn = in.readString();
        front_idler_pn = in.readString();
        rear_idler_pn = in.readString();
        single_flange_pn = in.readString();
        double_flange_pn = in.readString();
        first_sprocket_segment_pn = in.readString();
        seconds_sprocket_segment_pn = in.readString();
        created_at = in.readString();
    }

    public static final Creator<TrackinsReportGeneral> CREATOR = new Creator<TrackinsReportGeneral>() {
        @Override
        public TrackinsReportGeneral createFromParcel(Parcel in) {
            return new TrackinsReportGeneral(in);
        }

        @Override
        public TrackinsReportGeneral[] newArray(int size) {
            return new TrackinsReportGeneral[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_number() {
        return customer_number;
    }

    public void setCustomer_number(String customer_number) {
        this.customer_number = customer_number;
    }

    public String getJob_site() {
        return job_site;
    }

    public void setJob_site(String job_site) {
        this.job_site = job_site;
    }

    public String getGps_coordinate() {
        return gps_coordinate;
    }

    public void setGps_coordinate(String gps_coordinate) {
        this.gps_coordinate = gps_coordinate;
    }

    public String getPhoto_path() {
        return photo_path;
    }

    public void setPhoto_path(String photo_path) {
        this.photo_path = photo_path;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getModel_config() {
        return model_config;
    }

    public void setModel_config(String model_config) {
        this.model_config = model_config;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEquipment_number() {
        return equipment_number;
    }

    public void setEquipment_number(String equipment_number) {
        this.equipment_number = equipment_number;
    }

    public String getInspections_date() {
        return inspections_date;
    }

    public void setInspections_date(String inspections_date) {
        this.inspections_date = inspections_date;
    }

    public String getHour_meter_reading() {
        return hour_meter_reading;
    }

    public void setHour_meter_reading(String hour_meter_reading) {
        this.hour_meter_reading = hour_meter_reading;
    }

    public String getHour_meter_per_week() {
        return hour_meter_per_week;
    }

    public void setHour_meter_per_week(String hour_meter_per_week) {
        this.hour_meter_per_week = hour_meter_per_week;
    }

    public String getUnderfoot_impact() {
        return underfoot_impact;
    }

    public void setUnderfoot_impact(String underfoot_impact) {
        this.underfoot_impact = underfoot_impact;
    }

    public String getUnderfoot_abrasive() {
        return underfoot_abrasive;
    }

    public void setUnderfoot_abrasive(String underfoot_abrasive) {
        this.underfoot_abrasive = underfoot_abrasive;
    }

    public String getUnderfoot_moisture() {
        return underfoot_moisture;
    }

    public void setUnderfoot_moisture(String underfoot_moisture) {
        this.underfoot_moisture = underfoot_moisture;
    }

    public String getUnderfoot_packing() {
        return underfoot_packing;
    }

    public void setUnderfoot_packing(String underfoot_packing) {
        this.underfoot_packing = underfoot_packing;
    }

    public String getBushing_allowable_wear() {
        return bushing_allowable_wear;
    }

    public void setBushing_allowable_wear(String bushing_allowable_wear) {
        this.bushing_allowable_wear = bushing_allowable_wear;
    }

    public String getLink_allowable_wear() {
        return link_allowable_wear;
    }

    public void setLink_allowable_wear(String link_allowable_wear) {
        this.link_allowable_wear = link_allowable_wear;
    }

    public String getBushings_turned() {
        return bushings_turned;
    }

    public void setBushings_turned(String bushings_turned) {
        this.bushings_turned = bushings_turned;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getShoe_type() {
        return shoe_type;
    }

    public void setShoe_type(String shoe_type) {
        this.shoe_type = shoe_type;
    }

    public String getUnit_of_measure() {
        return unit_of_measure;
    }

    public void setUnit_of_measure(String unit_of_measure) {
        this.unit_of_measure = unit_of_measure;
    }

    public String getTrack_roller_quantities_sf() {
        return track_roller_quantities_sf;
    }

    public void setTrack_roller_quantities_sf(String track_roller_quantities_sf) {
        this.track_roller_quantities_sf = track_roller_quantities_sf;
    }

    public String getTrack_roller_quantities_df() {
        return track_roller_quantities_df;
    }

    public void setTrack_roller_quantities_df(String track_roller_quantities_df) {
        this.track_roller_quantities_df = track_roller_quantities_df;
    }

    public String getTrack_group_pn() {
        return track_group_pn;
    }

    public void setTrack_group_pn(String track_group_pn) {
        this.track_group_pn = track_group_pn;
    }

    public String getLink_assembly_pn() {
        return link_assembly_pn;
    }

    public void setLink_assembly_pn(String link_assembly_pn) {
        this.link_assembly_pn = link_assembly_pn;
    }

    public String getTrack_shoe_pn() {
        return track_shoe_pn;
    }

    public void setTrack_shoe_pn(String track_shoe_pn) {
        this.track_shoe_pn = track_shoe_pn;
    }

    public String getMaster_shoe_pn() {
        return master_shoe_pn;
    }

    public void setMaster_shoe_pn(String master_shoe_pn) {
        this.master_shoe_pn = master_shoe_pn;
    }

    public String getCarrier_roller_pn() {
        return carrier_roller_pn;
    }

    public void setCarrier_roller_pn(String carrier_roller_pn) {
        this.carrier_roller_pn = carrier_roller_pn;
    }

    public String getFront_idler_pn() {
        return front_idler_pn;
    }

    public void setFront_idler_pn(String front_idler_pn) {
        this.front_idler_pn = front_idler_pn;
    }

    public String getRear_idler_pn() {
        return rear_idler_pn;
    }

    public void setRear_idler_pn(String rear_idler_pn) {
        this.rear_idler_pn = rear_idler_pn;
    }

    public String getSingle_flange_pn() {
        return single_flange_pn;
    }

    public void setSingle_flange_pn(String single_flange_pn) {
        this.single_flange_pn = single_flange_pn;
    }

    public String getDouble_flange_pn() {
        return double_flange_pn;
    }

    public void setDouble_flange_pn(String double_flange_pn) {
        this.double_flange_pn = double_flange_pn;
    }

    public String getFirst_sprocket_segment_pn() {
        return first_sprocket_segment_pn;
    }

    public void setFirst_sprocket_segment_pn(String first_sprocket_segment_pn) {
        this.first_sprocket_segment_pn = first_sprocket_segment_pn;
    }

    public String getSeconds_sprocket_segment_pn() {
        return seconds_sprocket_segment_pn;
    }

    public void setSeconds_sprocket_segment_pn(String seconds_sprocket_segment_pn) {
        this.seconds_sprocket_segment_pn = seconds_sprocket_segment_pn;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(unique_id);
        dest.writeString(customer_name);
        dest.writeString(customer_number);
        dest.writeString(job_site);
        dest.writeString(gps_coordinate);
        dest.writeString(photo_path);
        dest.writeString(family);
        dest.writeString(model_config);
        dest.writeString(model);
        dest.writeString(equipment_number);
        dest.writeString(inspections_date);
        dest.writeString(hour_meter_reading);
        dest.writeString(hour_meter_per_week);
        dest.writeString(underfoot_impact);
        dest.writeString(underfoot_abrasive);
        dest.writeString(underfoot_moisture);
        dest.writeString(underfoot_packing);
        dest.writeString(bushing_allowable_wear);
        dest.writeString(link_allowable_wear);
        dest.writeString(bushings_turned);
        dest.writeString(notes);
        dest.writeString(shoe_type);
        dest.writeString(unit_of_measure);
        dest.writeString(track_roller_quantities_sf);
        dest.writeString(track_roller_quantities_df);
        dest.writeString(track_group_pn);
        dest.writeString(link_assembly_pn);
        dest.writeString(track_shoe_pn);
        dest.writeString(master_shoe_pn);
        dest.writeString(carrier_roller_pn);
        dest.writeString(front_idler_pn);
        dest.writeString(rear_idler_pn);
        dest.writeString(single_flange_pn);
        dest.writeString(double_flange_pn);
        dest.writeString(first_sprocket_segment_pn);
        dest.writeString(seconds_sprocket_segment_pn);
        dest.writeString(created_at);
    }
}