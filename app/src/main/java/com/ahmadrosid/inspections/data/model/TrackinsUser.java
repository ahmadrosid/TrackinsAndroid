package com.ahmadrosid.inspections.data.model;

/**
 * Created by ocittwo on 10/8/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class TrackinsUser {
    public String email;
    public String username;
    public String password;
}
