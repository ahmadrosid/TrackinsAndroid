package com.ahmadrosid.inspections.data;

/**
 * Created by ocittwo on 02/09/16.
 */
public class InspectionsDatabase{

    private Long id;
    public String title;
    public String picture;
    public String company;
    public String description;

    public InspectionsDatabase() {
    }

    public InspectionsDatabase(String title, String picture, String company, String description) {
        this.title = title;
        this.picture = picture;
        this.company = company;
        this.description = description;
    }

}
