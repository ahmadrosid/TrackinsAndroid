package com.ahmadrosid.inspections.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ocittwo on 27/09/16.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 *
 * © 2016 | App All Rights Reserved
 */
public class ResponseLogin implements Parcelable{

    public boolean success;
    public String message;
    public String api_token;

    public ResponseLogin(){}

    public boolean isSuccess() {
        return success;
    }

    protected ResponseLogin(Parcel in) {
        success = in.readByte() != 0;
        message = in.readString();
        api_token = in.readString();
    }

    public static final Creator<ResponseLogin> CREATOR = new Creator<ResponseLogin>() {
        @Override
        public ResponseLogin createFromParcel(Parcel in) {
            return new ResponseLogin(in);
        }

        @Override
        public ResponseLogin[] newArray(int size) {
            return new ResponseLogin[size];
        }
    };

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (success ? 1 : 0));
        parcel.writeString(message);
        parcel.writeString(api_token);
    }

    @Override public String toString() {
        return "Is success = " + success +
                " \nMessage = " + message +
                " \nApi token = " + api_token;
    }
}
